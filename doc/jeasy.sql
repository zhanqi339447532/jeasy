/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : jeasy

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 10/03/2020 16:24:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_um_log
-- ----------------------------
DROP TABLE IF EXISTS `t_um_log`;
CREATE TABLE `t_um_log` (
  `code` varchar(32) NOT NULL,
  `uid` varchar(32) DEFAULT NULL COMMENT 'uid',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `address` varchar(255) DEFAULT NULL COMMENT 'IP',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='操作日志';

-- ----------------------------
-- Records of t_um_log
-- ----------------------------
BEGIN;
INSERT INTO `t_um_log` VALUES ('060233DF782D40F6B45069BF630E1387', '1111', '查询', '127.0.0.1', '2020-03-10 01:35:04');
INSERT INTO `t_um_log` VALUES ('16BC6CC2963442AA8722CD979E954902', '1111', '查询', '0:0:0:0:0:0:0:1', '2020-03-09 23:03:35');
INSERT INTO `t_um_log` VALUES ('1D26203542AE440F98F8F36CD93FB6AD', '1111', '查询', '0:0:0:0:0:0:0:1', '2020-03-09 22:43:38');
INSERT INTO `t_um_log` VALUES ('23E47584C291478CBD16E38FCCC80569', '1111', '查询', '0:0:0:0:0:0:0:1', '2020-03-10 16:10:32');
INSERT INTO `t_um_log` VALUES ('358B91FFB5374B3599D352B08137AE4B', '1111', '查询', '0:0:0:0:0:0:0:1', '2020-03-09 23:11:02');
INSERT INTO `t_um_log` VALUES ('4EA23E3A2D074B89A4E809C32F823B04', '1111', '查询', '0:0:0:0:0:0:0:1', '2020-03-09 23:15:25');
INSERT INTO `t_um_log` VALUES ('699585D623B344FEB5ED23BA7312986D', '1111', '查询', '127.0.0.1', '2020-03-10 01:34:59');
INSERT INTO `t_um_log` VALUES ('6A242933BE664699BB642423C876A3D2', '1111', '查询', '127.0.0.1', '2020-03-10 01:29:54');
INSERT INTO `t_um_log` VALUES ('7541CF6EBA2241DE8101834695C6EA40', '1111', '查询', '127.0.0.1', '2020-03-10 01:34:58');
INSERT INTO `t_um_log` VALUES ('8401E815A9E84A029AC109EDB77FEC83', '1111', '查询', '0:0:0:0:0:0:0:1', '2020-03-09 23:21:05');
INSERT INTO `t_um_log` VALUES ('97E2B1AD7DD548868EB525597ACE83BF', '1111', '查询', '0:0:0:0:0:0:0:1', '2020-03-09 23:16:44');
INSERT INTO `t_um_log` VALUES ('AA9F7C46169945E8AF4602C916E5B9BF', '1111', '查询', '0:0:0:0:0:0:0:1', '2020-03-09 23:03:25');
INSERT INTO `t_um_log` VALUES ('B9DEF3FF4C124BCC9A0F50DC20A086B0', '1111', '查询', '0:0:0:0:0:0:0:1', '2020-03-09 23:19:16');
INSERT INTO `t_um_log` VALUES ('CE5E9EFC86A54D449171ABCB9C159E78', '1111', '查询', '0:0:0:0:0:0:0:1', '2020-03-09 23:19:09');
INSERT INTO `t_um_log` VALUES ('DDAFCC8A2B0F47C0A4CC08F89DDC594B', '1111', '查询', '0:0:0:0:0:0:0:1', '2020-03-10 15:40:10');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
