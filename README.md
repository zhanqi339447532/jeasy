# jeasy

一、 介绍
目前主流的框架，如Struts、Spring、jfinal都是基于servlet封装的。自己决定利用闲时封装一套框架出来，我将她取名jeasy。

二、 软件架构
软件架构说明

三、 使用说明
1.封装baseCotroller
实现自动接管，扫描，自定义拦截器

![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/145609_e413db81_1535643.png "屏幕截图.png")

2.封装了常用方法

![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/152128_3d30428c_1535643.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/152207_5cba04ef_1535643.png "屏幕截图.png")

3.定义接口示例如下：

![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/152308_766ab5ad_1535643.png "屏幕截图.png")

4.实现注解
  (1).实现@Inject自动注入
  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/150037_a982fb1d_1535643.png "屏幕截图.png")

  (2).实现@Interceptor自定义拦截器
      value参数是拦截器顺序，默认为顺序

  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/150258_011a0415_1535643.png "屏幕截图.png")

  (3).实现@Log(“查询”)自定义操作日志注解

  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/150347_b1450b9b_1535643.png "屏幕截图.png")

  (4).实现@IsNotNull(“xxx”)实现注解校验参数
      /**
     * 
     * 用法:("key->{name:空提示语}{type:数据类型}{v:idCard}")
     * type:{n=正整数;-n=负整数;d=正浮点;-d=负浮点;b=Boolean}
     * v:{idCard=身份证;phone=手机号码}验证
     * 配置文件扩展支持自定义
     * 说明:key前面加！不验证是否非空，只判断type v
     */
     自定义格式如下：

     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/150557_51730175_1535643.png "屏幕截图.png")

     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/152626_5669a116_1535643.png "屏幕截图.png")

  (5).实现@Configuration  @Bean自动加载插件、模块。
  为了提高扫描速度只扫描autoload包下面的，也可以自己自定义，如下：

  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/151124_6248f59a_1535643.png "屏幕截图.png") 

  用法示例如下：

  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/150719_632567b9_1535643.png "屏幕截图.png")

  (6).加入缓存类，只需要扫描一次

  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/151753_edfa1cc1_1535643.png "屏幕截图.png") 

  (7).代码生成器一键生成bean,model

  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/151331_479a56d0_1535643.png "屏幕截图.png")

  可以自定义路径等

  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/151503_bac5e8c2_1535643.png "屏幕截图.png")

  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/151520_69308b3f_1535643.png "屏幕截图.png")

四、性能测试
可以达到1万多每秒的好成绩
![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/145326_387a24f8_1535643.png "屏幕截图.png")


五、 参与贡献
占啟（339447532@qq.com）
