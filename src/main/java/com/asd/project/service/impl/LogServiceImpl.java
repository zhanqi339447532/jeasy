package com.asd.project.service.impl;

import com.asd.common.annotation.Service;
import com.asd.common.utils.MyUtil;
import com.asd.common.utils.Render;
import com.asd.project.bean.RequestBodyBean;
import com.asd.project.model.Log;
import com.asd.project.service.LogService;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhanqi
 * @since 2022/4/3 14:01
 */
@Service
public class LogServiceImpl implements LogService {

    /**
     * @param requestBodyBean
     * @return
     */
    @Override
    public Page query(RequestBodyBean requestBodyBean) {
        int page = requestBodyBean.getPage();
        int pageSize = requestBodyBean.getPageSize();
        String beginDate = requestBodyBean.getBeginDate();
        String endDate = requestBodyBean.getEndDate();
        List<Object> params = new ArrayList<Object>();
        String sql = " select t.* ";
        String sqlExceptSelect = " from t_um_log t ";

        if (!StringUtils.isBlank(beginDate)) {
            if (StringUtils.isBlank(endDate)) {
                endDate = MyUtil.createtimeStr("yyyy-MM-dd HH:mm:ss");
            }
            sqlExceptSelect += (params.size() == 0 ? " where " : " and ") + " createtime between ? and  ? ";
            params.add(beginDate);
            params.add(endDate);

        }
        sqlExceptSelect += "order by createtime desc ";
        Page<Log> pages = new Log().dao().paginate(page, pageSize, sql, sqlExceptSelect, params.toArray());
        return pages;
    }

    @Override
    public boolean setLog(String uid, String content, String address) {
        Log log = new Log();
        log.setCode(MyUtil.getUUID32());
        log.setUid(uid);
        log.setContent(content);
        log.setAddress(address);
        log.setCreatetime(null);
        return log.save();
    }

}
