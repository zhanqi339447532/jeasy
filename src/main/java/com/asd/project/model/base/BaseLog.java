package com.asd.project.model.base;

import com.alibaba.fastjson.annotation.JSONField;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({ "serial", "unchecked" })
public abstract class BaseLog<M extends BaseLog<M>> extends Model<M> implements IBean {

	public void setCode(String code) {
		set("code", code);
	}

	public String getCode() {
		return getStr("code");
	}

	public void setUid(String uid) {
		set("uid", uid);
	}

	public String getUid() {
		return getStr("uid");
	}

	public void setContent(String content) {
		set("content", content);
	}

	public String getContent() {
		return getStr("content");
	}

	public void setAddress(String address) {
		set("address", address);
	}

	public String getAddress() {
		return getStr("address");
	}

	public void setCreatetime(String createtime) {
		set("createtime", createtime);

	}

	public String getCreatetime() {
		return get("createtime");
	}

}
