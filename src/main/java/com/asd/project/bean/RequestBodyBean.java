package com.asd.project.bean;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author zhanqi
 * @since 2022/5/7 0:19
 */
@Data
@XmlRootElement //接收xml https://blog.csdn.net/m0_37983376/article/details/73930285
public class RequestBodyBean {
    private Integer page;
    private Integer pageSize;
    private String beginDate;
    private String endDate;
}
