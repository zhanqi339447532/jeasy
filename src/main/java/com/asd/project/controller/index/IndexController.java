/**
 *   ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 *　　┃　　　┃神兽保佑
 *　　┃　　　┃代码无BUG！
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　    ┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　　　┗┻┛　┗┻┛
**/
package com.asd.project.controller.index;

import javax.servlet.annotation.WebServlet;

import com.asd.common.annotation.*;
import com.asd.common.controller.BaseController;
import com.asd.project.bean.RequestBodyBean;
import com.asd.project.service.LogService;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

/**
 * @author zhanqi
 * @date 2020-03-28
 */
@SuppressWarnings("serial")
@WebServlet(urlPatterns = { "/" }, name = "首页",asyncSupported=true)
public class IndexController extends BaseController {

	@Autowired
	LogService logService;

	public String index() {
		return "/我是首页接口index";
	}

	@Log("list查询")
	public RequestBodyBean list(@RequestBody RequestBodyBean requestBodyBean,@RequestParam("val") String val) {
		String data = getParam("data");
		System.out.println(val);
		return requestBodyBean;

	}

	@Log("query查询")
	public Page query(@RequestBody RequestBodyBean requestBodyBean) {
		return logService.query(requestBodyBean);

	}

}
