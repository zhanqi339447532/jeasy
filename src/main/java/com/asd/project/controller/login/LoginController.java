/**
 *   ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 *　　┃　　　┃神兽保佑
 *　　┃　　　┃代码无BUG！
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　    ┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　　　┗┻┛　┗┻┛
**/
package com.asd.project.controller.login;

import javax.servlet.annotation.WebServlet;

import com.asd.common.annotation.RequestParam;
import com.asd.common.controller.BaseController;

import java.util.Map;

/**
 * 登陆接口
 *
 * @author zhanqi
 * @date 2020年03月05日
 */
@SuppressWarnings("serial")
@WebServlet(urlPatterns = { "/login" }, name = "登陆",asyncSupported=true)
public class LoginController extends BaseController {

	public String index() {
		return "/我是登陆接口index";
	}

	public String login() {
		return "/我是登陆接口login";
	}

}
