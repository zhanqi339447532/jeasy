package com.asd.project;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;

import java.io.File;

/**
 * @author zhanqi
 * @since 2022/5/8 2:43
 */
public class ServerApplication {
    public static void main(String[] args) {
        ServerApplication.run();
    }

    public static void run() {
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(8084);
        Connector connector=tomcat.getConnector();
        connector.setMaxPostSize(20971520);//20m
        connector.setMaxSavePostSize(20971520);
        tomcat.setConnector(connector);
        // 创建 WebApp
        File webAppFile = new File("src/main/webapp");
        if(!webAppFile.exists()){
            webAppFile= new File("webapp");
        }
        Context context=tomcat.addWebapp("/", webAppFile.getAbsolutePath());
        WebResourceRoot resources = new StandardRoot(context);
        File classesFile = new File("target/classes");
        resources.addPreResources(new DirResourceSet(resources, "/WEB-INF/classes", classesFile.getParent(), "/"));
        context.setResources(resources);
        try {
            tomcat.start();
            tomcat.getServer().await();
        } catch (LifecycleException e) {
            e.printStackTrace();
        }

    }

}
