/**
 *   ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 *　　┃　　　┃神兽保佑
 *　　┃　　　┃代码无BUG！
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　    ┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　　　┗┻┛　┗┻┛
**/
package com.asd.common.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;

/**
 * @author zhanqi
 * @date 2019年6月29日
 */
public class MyUtil {

	public static final String PATTERN_SSS = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String PATTERN_SS = "yyyy-MM-dd HH:mm:ss";

	public static String getUUID32() {
		return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
	}

	public static String CheckCode() {
		// String str="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String str = "0123456789";
		StringBuilder sb = new StringBuilder(4);
		for (int i = 0; i < 4; i++) {
			// public int nextInt(int n)
			// 该方法的作用是生成一个随机的int值，该值介于[0,n)的区间，也就是0到n之间的随机int值，包含0而不包含n。
			char ch = str.charAt(new Random().nextInt(str.length()));
			sb.append(ch);
		}
		return sb.toString();

	}

	public static String mobilePass(String phone) {
		String phoneNumber = phone.substring(0, 3) + "****" + phone.substring(7, phone.length());
		return phoneNumber;
	}

	public static Timestamp createtime() {
		Date time = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_SSS);
		String createtime = sdf.format(time);
		Timestamp date = Timestamp.valueOf(createtime);
		return date;
	}
	public static Timestamp createtime(String pattern) {
		Date time = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		String createtime = sdf.format(time);
		Timestamp date = Timestamp.valueOf(createtime);
		return date;
	}


	public static String createtimeStr() {
		Date time = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_SSS);
		String createtime = sdf.format(time);
		return createtime;
	}

	public static String createtimeStr(String format) {
		Date time = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String createtime = sdf.format(time);
		return createtime;
	}

	public static ArrayList<Integer> parseIntegersList(List<String> StringList) {
		ArrayList<Integer> IntegerList = new ArrayList<Integer>();
		for (String x : StringList) {
			Integer z = Integer.parseInt(x);
			IntegerList.add(z);
		}
		return IntegerList;
	}

	public static ArrayList<String> parseStrList(List<Integer> StringList) {
		ArrayList<String> StrList = new ArrayList<String>();
		for (Integer x : StringList) {
			String z = String.valueOf(x);
			StrList.add(z);
		}
		return StrList;
	}

	// 计算日期相差多少秒
	public static long timeSubtraction(String start) {
		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date startDate = null;
		Date endDate = null;
		try {
			Date d = new Date();
			System.out.println(inputFormat.format(d));
			// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			startDate = inputFormat.parse(start);
			endDate = inputFormat.parse(inputFormat.format(d));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		// 得到两个日期对象的总毫秒数
		long firstDateMilliSeconds = startDate.getTime();
		long secondDateMilliSeconds = endDate.getTime();

		// 得到两者之差
		long firstMinusSecond = secondDateMilliSeconds - firstDateMilliSeconds;
		// 得到秒
		long totalSeconds = (firstMinusSecond / 1000);
		// System.out.println(totalSeconds);
		return totalSeconds;
	}

	/**
	 * "gfhgfhgf,"去结尾字符，没有就不去 spl=',' 最后一位的字符去掉
	 *
	 */
	public static String groupConcatSplit(String str, String spl) {
		if (str != null) {
			if (str.substring(str.length() - 1, str.length()).equals(spl)) {
				str = str.substring(0, str.length() - 1);
			}
		}
		return str;
	}

	// 判断是否是json
	public static boolean isJson(String content) {
		try {
			JSON.parseObject(content);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static int getTimeType() {
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat("HH");
		String str = df.format(date);
		int a = Integer.parseInt(str);
		if (a >= 0 && a <= 6) {

			System.out.println("凌晨");
			return 1;
		}
		if (a > 6 && a <= 12) {
			System.out.println("上午");
			return 2;
		}
		if (a > 12 && a <= 13) {
			System.out.println("中午");
			return 3;
		}
		if (a > 13 && a <= 18) {
			System.out.println("下午");
			return 4;
		}
		if (a > 18 && a <= 24) {
			System.out.println("晚上");
			return 5;
		}
		return 0;
	}

	public static String getTriggerStatesCN(String key) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("BLOCKED", "阻塞");
		map.put("COMPLETE", "完成");
		map.put("ERROR", "出错");
		map.put("NONE", "不存在");
		map.put("NORMAL", "正常");
		map.put("PAUSED", "暂停");

		map.put("4", "阻塞");
		map.put("2", "完成");
		map.put("3", "出错");
		map.put("-1", "不存在");
		map.put("0", "正常");
		map.put("1", "暂停");
		/*
		 * **STATE_BLOCKED 4 阻塞 STATE_COMPLETE 2 完成 STATE_ERROR 3 错误 STATE_NONE -1 不存在
		 * STATE_NORMAL 0 正常 STATE_PAUSED 1 暂停
		 ***/
		return map.get(key);
	}

	public static String formatRatio(Double a, Double b) {
		String ratio = "0.00";
		if (b > 0) {
			ratio = String.format("%.2f", (a / b) * 100);
		}

		if (ratio.equals("100.00")) {
			ratio = "100";
		}
		return ratio;

	}

	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
}
