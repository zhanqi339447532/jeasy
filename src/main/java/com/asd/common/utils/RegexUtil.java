package com.asd.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil {
	public static boolean isPhone(String phone) {
		String regex = "^((13[0-9])|(14[0-9])|(15([0-9]))|(16[0-9])|(17[0,1,3,5,6,7,8])|(18[0-9])|(19[0-9]))\\d{8}$";
		if (phone.length() != 11) {
			return false;
		} else {
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(phone);
			boolean isMatch = m.matches();
			return isMatch;
		}
	}

	public static boolean Regex(String regex, String str) {
		// String regex ="^[A-Za-z0-9]+$";
		return str.matches(regex);
	}

	public static String getT(String regex, String str) {
		Pattern pattern = Pattern.compile(regex);
		Matcher m = pattern.matcher(str);
		if (m.find()) {
			return m.group(0);

		}
		return null;
	}

	/**
	 * @param regex 正则表达式字符串
	 * @param str   要匹配的字符串
	 * @return 如果str 符合 regex的正则表达式格式,返回true, 否则返回 false;
	 */
	public static boolean match(String regex, String str) {
		if (regex == null || str == null) {
			System.out.println("prama 没有取到值！！");
			return true;
		}
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}

	// 匹配名称
	public static String getPattern(String patternStr, String str) {
		// 编译正则表达式
		Pattern pattern = Pattern.compile(patternStr);
		// 开始匹配
		Matcher matcher = pattern.matcher(str);
		while (matcher.find()) {
			return matcher.group(1);
		}
		return "";
	}
}
