/**
 *   ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 *　　┃　　　┃神兽保佑
 *　　┃　　　┃代码无BUG！
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　    ┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　　　┗┻┛　┗┻┛
 **/
package com.asd.common.interceptors;

import java.lang.reflect.Method;

import com.asd.common.annotation.Aspect;
import com.asd.common.annotation.Interceptor;
import org.apache.commons.lang3.StringUtils;
import com.asd.common.annotation.IsNotNull;
import com.asd.common.controller.BaseController;
import com.asd.common.interceptors.bean.InvocationBean;
import com.asd.common.interceptors.enums.CheckTypeEnum;
import com.asd.common.interceptors.enums.CheckEnum;
import com.asd.common.interceptors.enums.IsNotNullEnum;
import com.asd.common.utils.RegexUtil;

/**
 * 自定义参数拦截器 加@Interceptors注解用于扫描
 *
 * @author zhanqi
 * @date 2020-03-08
 */

@Aspect(2)
public class IsNotNullAspect extends BaseAspect {
	@Override
	public boolean exe(InvocationBean inv) {
		BaseController ct = inv.getCt();
		Method method = inv.getMethod();
		// 参数注解
		IsNotNull isnull = method.getAnnotation(IsNotNull.class);
		if (isnull != null) {
			String msg = verifyData(ct, isnull);
			if (msg != null) {
				ct.renderFail(msg);
				return false;
			}
		}
		return true;
	}

	protected String verifyData(BaseController ct, IsNotNull isnull) {
		String strs = isnull.value();
		String[] str = strs.split(IsNotNullEnum.SPLIT_STR.getValue());
		if (str.length > 0) {
			for (int i = 0; i < str.length; i++) {
				String value = str[i].trim();
				String key = patternKey(value);

				// 匹配 {name:xxx}
				String name = RegexUtil.getPattern(IsNotNullEnum.NAME.getValue(), value);
				String type = RegexUtil.getPattern(IsNotNullEnum.TYPE.getValue(), value);
				String v = RegexUtil.getPattern(IsNotNullEnum.V.getValue(), value);
				String msg;
				boolean notNull = false;
				if (key.startsWith(IsNotNullEnum.STARTSWITH_STR.getValue())) {
					notNull = true;
					key = key.substring(1);
				}
				if (StringUtils.isBlank(name)) {
					msg = key;
				} else {
					msg = name;
				}
				String paraValue = ct.getParam(key);
				if (!notNull && StringUtils.isBlank(paraValue)) {
					return msg.concat(IsNotNullEnum.ERROR_MSG.getValue());
				}
				if (!StringUtils.isBlank(paraValue)) {
					String checkType = CheckTypeEnum.getTypeEnum(type, paraValue, msg);
					if (checkType != null)
						return checkType;

					String checkValue = CheckEnum.getEnum(v, paraValue, msg);
					if (checkValue != null)
						return checkValue;

				}

			}
		}
		return null;
	}

	protected String patternKey(String key) {
		String[] keys = key.split(IsNotNullEnum.PATTERN_KEY.getValue());
		if (keys.length > 0) {
			return keys[0];
		}
		return key;
	}

}
