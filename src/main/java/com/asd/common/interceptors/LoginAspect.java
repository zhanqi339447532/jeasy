/**
 * ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃代码无BUG！
 * 　　┃　　　┗━━━┓
 * 　　┃　　　　　　　┣┓
 * 　    ┃　　　　　　　┏┛
 * 　　┗┓┓┏━┳┓┏┛
 * 　　　┃┫┫　┃┫┫
 * 　　　┗┻┛　┗┻┛
 **/
package com.asd.common.interceptors;

import com.asd.common.annotation.Aspect;
import com.asd.common.annotation.Interceptor;
import com.asd.common.utils.PropKit;
import org.apache.commons.lang3.StringUtils;

import com.asd.common.controller.BaseController;
import com.asd.common.interceptors.bean.InvocationBean;
import com.asd.common.utils.Render;

import java.util.Arrays;

/**
 * 自定义登陆拦截器 加@Interceptors注解用于扫描
 *
 * @author zhanqi
 * @date 2020-03-09
 */

@Aspect(1)
public class LoginAspect extends BaseAspect {
    @Override
    public boolean exe(InvocationBean inv) {
        BaseController ct = inv.getCt();
        String sPath = ct.getReq().getServletPath();
        String token = ct.getParam("token");
        String loginAspectUrl = PropKit.get("loginAspectUrl", "/,/login");
        String[] loginAspectUrls = loginAspectUrl.split(",");
        if (Arrays.asList(loginAspectUrls).contains(sPath)) {
            return true;
        }
        // 此处自定义
        if (StringUtils.isBlank(token)) {
            Render.renderJson(ct.getResp(), 403, "无权限");
            return false;
        }
        return true;
    }

}
