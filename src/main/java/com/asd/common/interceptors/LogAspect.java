/**
 *   ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 *　　┃　　　┃神兽保佑
 *　　┃　　　┃代码无BUG！
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　    ┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　　　┗┻┛　┗┻┛
 **/
package com.asd.common.interceptors;

import java.lang.reflect.Method;

import com.asd.common.annotation.Aspect;
import com.asd.common.annotation.Autowired;
import com.asd.common.annotation.Interceptor;
import com.asd.common.annotation.Log;
import com.asd.common.controller.BaseController;
import com.asd.common.interceptors.bean.InvocationBean;
import com.asd.common.utils.MyUtil;
import com.asd.project.service.LogService;
import com.asd.project.service.impl.LogServiceImpl;

/**
 * 自定义日志拦截器 加@Interceptors注解用于扫描
 *
 * @author zhanqi
 * @date 2020-03-09
 */

@Aspect(4)
public class LogAspect extends BaseAspect {
	@Autowired
	private LogService logService;

	@Override
	public boolean exe(InvocationBean inv) {
		BaseController ct = inv.getCt();
		Method method = inv.getMethod();
		// 操作日志注解
		Log log = method.getAnnotation(Log.class);
		if (log != null) {
			String content = log.value();
			String uid = (String) ct.getSessionAttr("uid");
			if (uid == null) {
				uid="1";
			}
			return logService.setLog(uid, content, MyUtil.getIpAddr(ct.getReq()));
		}
		return true;
	}

}
