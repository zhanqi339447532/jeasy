/**
 *   ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 *　　┃　　　┃神兽保佑
 *　　┃　　　┃代码无BUG！
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　    ┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　　　┗┻┛　┗┻┛
**/
package com.asd.common.interceptors.bean;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.asd.common.controller.BaseController;

/**
 * @author zhanqi
 * @date 2020-03-08
 */
public class InvocationBean {
	private Class<?> clazz;
	private Method method;
	private HttpServletRequest req;
	private HttpServletResponse resp;
	private BaseController ct;
	private String bodyStr;
	private Object[] methodParameters;

	/**
	 * @return clazz
	 */
	public Class<?> getClazz() {
		return clazz;
	}

	/**
	 * @param clazz 要设置的 clazz
	 */
	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	/**
	 * @return method
	 */
	public Method getMethod() {
		return method;
	}

	/**
	 * @param method 要设置的 method
	 */
	public void setMethod(Method method) {
		this.method = method;
	}

	/**
	 * @return req
	 */
	public HttpServletRequest getReq() {
		return req;
	}

	/**
	 * @param req 要设置的 req
	 */
	public void setReq(HttpServletRequest req) {
		this.req = req;
	}

	/**
	 * @return resp
	 */
	public HttpServletResponse getResp() {
		return resp;
	}

	/**
	 * @param resp 要设置的 resp
	 */
	public void setResp(HttpServletResponse resp) {
		this.resp = resp;
	}

	/**
	 * @return ct
	 */
	public BaseController getCt() {
		return ct;
	}

	/**
	 * @param ct 要设置的 ct
	 */
	public void setCt(BaseController ct) {
		this.ct = ct;
	}


	public String getBodyStr() {
		return bodyStr;
	}

	public void setBodyStr(String bodyStr) {
		this.bodyStr = bodyStr;
	}

	public Object[] getMethodParameters() {
		return methodParameters;
	}

	public void setMethodParameters(Object[] methodParameters) {
		this.methodParameters = methodParameters;
	}
}
