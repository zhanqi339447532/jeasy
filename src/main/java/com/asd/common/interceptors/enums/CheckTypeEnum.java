/**
 * ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃代码无BUG！
 * 　　┃　　　┗━━━┓
 * 　　┃　　　　　　　┣┓
 * 　    ┃　　　　　　　┏┛
 * 　　┗┓┓┏━┳┓┏┛
 * 　　　┃┫┫　┃┫┫
 * 　　　┗┻┛　┗┻┛
 **/
package com.asd.common.interceptors.enums;

import lombok.extern.slf4j.Slf4j;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @IsNotNull枚举数据类型检查
 * @author zhanqi
 * @date 2020-03-10
 */
@Slf4j
public enum CheckTypeEnum {
    n {
        @Override
        public String method(String paraValue, String msg) {
            if (!match("[0-9]\\d*", paraValue)) {
                return msg.concat(IsNotNullEnum.ERROR_N_MSG.getValue());
            }
            return null;
        }
    },
    _n {
        @Override
        public String method(String paraValue, String msg) {
            if (!match("-[1-9]\\d*", paraValue)) {
                return msg.concat(IsNotNullEnum.ERROR_N2_MSG.getValue());
            }
            return null;
        }
    },
    d {
        @Override
        public String method(String paraValue, String msg) {
            if (!match("[\\d]+\\.[\\d]+", paraValue)) {
                return msg.concat(IsNotNullEnum.ERROR_D_MSG.getValue());
            }
            return null;
        }
    },
    _d {
        @Override
        public String method(String paraValue, String msg) {
            if (!match("^(-?\\d+)(\\.\\d+)?$", paraValue)) {
                return msg.concat(IsNotNullEnum.ERROR_D2_MSG.getValue());
            }
            return null;
        }
    },
    b {
        @Override
        public String method(String paraValue, String msg) {
            Class<? extends Object> className = paraValue.getClass();
            if (!className.equals(java.lang.Boolean.class)) {
                return msg.concat(IsNotNullEnum.ERROR_B_MSG.getValue());
            }
            return null;
        }
    };

    /**
     * @param regex 正则表达式字符串
     * @param str   要匹配的字符串
     * @return 如果str 符合 regex的正则表达式格式,返回true, 否则返回 false;
     */
    protected boolean match(String regex, String str) {
        if (regex == null || str == null) {
            log.error("param 没有取到值！！");
            return true;
        }
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }

    public abstract String method(String paraValue, String msg);

    private static String check(String type, String paraValue, String msg) {
        switch (type) {
            case "n":
                return CheckTypeEnum.n.method(paraValue, msg);
            case "-n":
                return CheckTypeEnum._n.method(paraValue, msg);
            case "d":
                return CheckTypeEnum.d.method(paraValue, msg);
            case "-d":
                return CheckTypeEnum._d.method(paraValue, msg);
            case "b":
                return CheckTypeEnum.b.method(paraValue, msg);
            default:
                break;
        }
        return null;
    }

    private static String[] CKECK_TYPE = {"n", "-n", "d", "-d", "b"};

    public static String getTypeEnum(String type, String paraValue, String msg) {
        for (String str : CKECK_TYPE) {
            if (str.equals(type)) {
                String result = check(type, paraValue, msg);
                return result;

            }
        }

        return null;
    }
}
