/**
 *   ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 *　　┃　　　┃神兽保佑
 *　　┃　　　┃代码无BUG！
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　    ┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　　　┗┻┛　┗┻┛
**/
package com.asd.common.interceptors.enums;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.asd.common.utils.CheckCID;
import com.asd.common.utils.PropKit;
import com.asd.common.utils.RegexUtil;

/**
 * @IsNotNull枚举数据类型检查
 * @author zhanqi
 * @date 2020-03-10
 */
public enum CheckEnum {

	idCard {
		@Override
		public String method(String paraValue, String msg) {
			if (!CheckCID.isIdCard(paraValue)) {
				return msg.concat(IsNotNullEnum.ERROR_IDCARD_MSG.getValue());
			}
			return null;
		}
	},
	phone {
		@Override
		public String method(String paraValue, String msg) {
			if (!RegexUtil.isPhone(paraValue)) {
				return msg.concat(IsNotNullEnum.ERROR_PHONE_MSG.getValue());
			}
			return null;
		}
	};

	public abstract String method(String paraValue, String msg);

	private static String[] CKECK_V = { "idCard", "phone" };

	private static String check(String v, String paraValue, String msg) {
		switch (v) {
		case "idCard":
			return CheckEnum.idCard.method(paraValue, msg);
		case "phone":
			return CheckEnum.phone.method(paraValue, msg);
		default:
			break;
		}
		return null;
	}

	public static String getEnum(String v, String paraValue, String msg) {
		for (String str : CKECK_V) {
			if (str.equals(v)) {
				String result = check(v, paraValue, msg);
				return result;

			}
		}
		// 配置文件正则扩展
		String vType = PropKit.get("vType", "[]").trim();
		JSONArray arr = JSON.parseArray(vType);
		if (arr.size() > 0) {
			for (int i = 0; i < arr.size(); i++) {
				JSONObject obj = arr.getJSONObject(i);
				String type = obj.getString("type");
				String regex = obj.getString("regex");
				msg = obj.getString("msg");
				if (type.equals(v) && !RegexUtil.match(regex, paraValue)) {
					return v.concat(msg);
				}
			}
		}
		return null;
	}
}
