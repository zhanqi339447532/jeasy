/**
 *   ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 *　　┃　　　┃神兽保佑
 *　　┃　　　┃代码无BUG！
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　    ┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　　　┗┻┛　┗┻┛
**/
package com.asd.common.interceptors.enums;

/**
 * @IsNotNull枚举常量
 * @author zhanqi
 * @date 2020-03-10
 */
public enum IsNotNullEnum {
	ERROR_MSG("不能为空"), ERROR_IDCARD_MSG("号码不正确"), ERROR_PHONE_MSG("号码不正确"), ERROR_N_MSG("请返回正整类型"),
	ERROR_N2_MSG("请返回负整类型"), ERROR_D_MSG("请返回正浮点类型"), ERROR_D2_MSG("请返回负浮点类型"), ERROR_B_MSG("请返回Boolean类型"),
	NAME("\\{name:(.*?)\\}"), TYPE("\\{type:(.*?)\\}"), V("\\{v:(.*?)\\}"), SPLIT_STR(","), STARTSWITH_STR("!"),
	PATTERN_KEY("->");

	private String Str;

	private IsNotNullEnum(String Str) {
		this.Str = Str;
	}

	public String getValue() {
		return Str;
	}
}
