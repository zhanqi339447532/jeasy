/**
 * ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃代码无BUG！
 * 　　┃　　　┗━━━┓
 * 　　┃　　　　　　　┣┓
 * 　    ┃　　　　　　　┏┛
 * 　　┗┓┓┏━┳┓┏┛
 * 　　　┃┫┫　┃┫┫
 * 　　　┗┻┛　┗┻┛
 **/
package com.asd.common.listener;

import com.asd.common.annotation.Aspect;
import com.asd.common.annotation.Configuration;
import com.asd.common.annotation.Interceptor;
import com.asd.common.annotation.Service;
import com.asd.common.listener.reflex.BeanReflexMemory;
import com.asd.common.utils.ClassUtil;
import com.asd.common.utils.Info;
import com.asd.common.utils.Prop;
import com.asd.common.utils.PropKit;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Set;

/**
 * 监听器 目前实现自动扫描注解启动第三方插件
 *
 * @author zhanqi
 * @date 2020年03月06日
 */

@WebListener
public class StartListener implements ServletContextListener {
    public static Prop p;

    static void loadConfig() {
        if (p == null) {
            p = PropKit.use("config.properties").appendIfExists("config-pro.properties");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        Info.out(getClass(), "监听器");
        loadConfig();
        // 扫描插件并启动
        of();
    }

    private void of() {
        ClassUtil classUtil = new ClassUtil();
        Set<Class<?>> clazzs = classUtil.getClasses(PropKit.get("packageName"));
        for (Class<?> clazz : clazzs) {
            Configuration configuration = clazz.getAnnotation(Configuration.class);
            if (configuration != null) {
                // 获取所有方法
                Method[] methods = clazz.getDeclaredMethods();
                if (methods == null) {
                    continue;
                }
                findBean(methods, clazz);
            }
            Service service = clazz.getAnnotation(Service.class);
            if (service != null) {
                BeanReflexMemory.of().setBean(clazz.getName(), clazz);
            }
            Interceptor Interceptor = clazz.getAnnotation(Interceptor.class);
            if (Interceptor != null) {
                BeanReflexMemory.of().setBean(clazz.getName(), clazz);
            }
            Aspect aspect = clazz.getAnnotation(Aspect.class);
            if (aspect != null) {
                BeanReflexMemory.of().setBean(clazz.getName(), clazz);
            }
            WebServlet webServlet = clazz.getAnnotation(WebServlet.class);
            if (webServlet != null) {
                BeanReflexMemory.of().setBean(clazz.getName(), clazz);
            }
        }

    }

    // 查询@Bean并运行方法
    private void findBean(Method[] methods, Class<?> clazz) {
        for (Method method : methods) {
            Annotation[] annotations = method.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation.annotationType().getSimpleName().equals("Bean")) {
                    // System.out.println(method.getName());
                    try {
                        method.invoke(clazz.newInstance());
                        BeanReflexMemory.of().setBean(clazz.getName(), clazz);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        }
    }
}
