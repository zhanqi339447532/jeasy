package com.asd.common.listener.reflex;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * bean扫描储存器
 * @author zhanqi
 * @since 2022/4/3 15:08
 */
public class BeanReflexMemory {
    public static Map<String, Class<?>> clazzs=new ConcurrentHashMap<>();
    private static BeanReflexMemory beanReflexMemory = new BeanReflexMemory();
    private BeanReflexMemory(){}
    public static BeanReflexMemory of(){
        return beanReflexMemory;
    }
    public Class<?> setBean(String className, Class<?> cs){
        Class<?> findCs=clazzs.get(className);
        if(findCs!=null){
            throw new RuntimeException("bean注入失败！！,存在相同的名称["+className+"]");
        }
       return clazzs.put(className,cs);
    }
    public Class<?> setRemove(Class<?> cs){
        return clazzs.remove(cs);
    }
    public Class<?> getBean(String className){
       return clazzs.get(className);
    }
    public Map<String, Class<?>> getAllBean(){
        return clazzs;
    }
}
