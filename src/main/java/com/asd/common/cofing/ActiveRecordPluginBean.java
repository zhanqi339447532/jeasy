/**
 *   ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 *　　┃　　　┃神兽保佑
 *　　┃　　　┃代码无BUG！
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　    ┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　　　┗┻┛　┗┻┛
**/
package com.asd.common.cofing;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.asd.common.annotation.Bean;
import com.asd.common.annotation.Configuration;
import com.asd.common.utils.Info;
import com.asd.common.utils.PropKit;
import com.asd.project.model._MappingKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.dialect.SqlServerDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.template.source.ClassPathSourceFactory;

/**
 * 自动装载jfianl插件
 *
 * @author zhanqi
 * @date 2020-03-07
 */
@Configuration
public class ActiveRecordPluginBean {
	EhCachePlugin ehc = new EhCachePlugin();

	/**
	 * 获取数据库插件
	 */
	public static DruidPlugin getDruidPlugin() {
		return new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"),PropKit.get("password"));
	}

	@Bean
	public void of() {
		String dbType = PropKit.get("dbType");
		// 配置数据库连接池插件
		DruidPlugin dbPlugin = getDruidPlugin();
		WallFilter wallFilter = new WallFilter(); // 加强数据库安全
		wallFilter.setDbType(dbType);
		dbPlugin.addFilter(wallFilter);
		dbPlugin.addFilter(new StatFilter()); // 添加 StatFilter 才会有统计数据

		// 数据映射 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(dbPlugin);
		// sql模板
		arp.getEngine().setSourceFactory(new ClassPathSourceFactory());
		arp.addSqlTemplate("/sql/sqlTemplate.sql");
		arp.setShowSql(PropKit.getBoolean("devMode"));
		if("sqlserver".equals(dbType)){
			arp.setDialect(new SqlServerDialect());
			dbPlugin.setDriverClass("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		}else if("mysql".equals(dbType)){
			arp.setDialect(new MysqlDialect());
			dbPlugin.setDriverClass("com.mysql.jdbc.Driver");
		}

		_MappingKit.mapping(arp);
		dbPlugin.start();
		arp.start();
		ehc.start();
		Info.out(getClass(), "数据OEM");
	}
}
