/**
 *   ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 *　　┃　　　┃神兽保佑
 *　　┃　　　┃代码无BUG！
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　    ┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　　　┗┻┛　┗┻┛
 * @Package: com.chuyun.aop.common
 * @author: zhanqi
 * @date: 2018-7-19 下午2:48:33
 */
package com.asd.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * 用法:("key->{name:空提示语}{type:数据类型}{v:idCard}") <br/>
 * type:{n=正整数;-n=负整数;d=正浮点;-d=负浮点;b=Boolean}<br/>
 * v:{idCard=身份证;phone=手机号码}验证<br/>
 * 其他扩展后期再加<br/>
 * 说明:key前面加！不验证是否非空，只判断type v<br/>
 *
 * @author zhanqi
 * @date 2020年02月13日
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
public @interface IsNotNull {
	String value();

}
