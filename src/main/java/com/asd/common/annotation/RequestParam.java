package com.asd.common.annotation;

import java.lang.annotation.*;

/**
 * 语法：@RequestParam(value=”参数名”,required=”true/false”,defaultValue=””)
 * value：参数名
 * required：是否包含该参数，默认为true，表示该请求路径中必须包含该参数，如果不包含就报错。
 * defaultValue：默认参数值，如果设置了该值，required=true将失效，自动为false,如果没有传该参数，就使用默认值
 * @author zhanqi
 * @since 2022/4/8 13:27
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestParam {

    String value() default "";

    String name() default "";

    boolean required() default true;

    String defaultValue() default "";
}
