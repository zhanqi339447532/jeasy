package com.asd.common.annotation;

import java.lang.annotation.*;

/**
 * @author zhanqi
 * @since 2022/5/7 15:06
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface Aspect {
    int value() default 0;
}
