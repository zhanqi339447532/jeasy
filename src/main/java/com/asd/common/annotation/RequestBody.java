package com.asd.common.annotation;

import java.lang.annotation.*;

/**
 * @author zhanqi
 * @since 2022/5/6 23:56
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface RequestBody {
}
