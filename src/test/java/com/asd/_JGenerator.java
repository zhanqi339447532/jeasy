package com.asd;

import javax.sql.DataSource;

import com.asd.common.utils.Prop;
import com.asd.common.utils.PropKit;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.dialect.SqlServerDialect;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;

public class _JGenerator {
	private static Prop p;

	// 先加载开发环境配置，再追加生产环境的少量配置覆盖掉开发环境配置
	static void loadConfig() {
		if (p == null) {
			p = PropKit.use("config.properties").appendIfExists("_JGenerator.properties");
		}
	}

	/**
	 * 获取数据库插件 抽取成独立的方法，便于重用该方法，减少代码冗余
	 */
	public static DataSource getDataSource() {
		loadConfig();
		DruidPlugin druidPlugin = new DruidPlugin(p.get("jdbcUrl"), p.get("user"), p.get("password"));
		druidPlugin.start();
		return druidPlugin.getDataSource();
	}

	public static void main(String[] args) {
		loadConfig();
		String dbType = p.get("dbType");
		// base model 所使用的包名
		String baseModelPackageName = p.get("baseModelPackageName");
		// base model 文件保存路径
		String baseModelOutputDir = PathKit.getWebRootPath() + p.get("baseModelOutputDir");

		// model 所使用的包名 (MappingKit 默认使用的包名)
		String modelPackageName = p.get("modelPackageName");
		// model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
		String modelOutputDir = baseModelOutputDir + p.get("modelOutputDir");

		// 创建生成器
		Generator generator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName,
				modelOutputDir);
		// 在 getter、setter 方法上生成字段备注内容
		generator.setGenerateRemarks(true);
		// 设置是否生成字典文件
		generator.setGenerateDataDictionary(true);
		// 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非
		generator.setRemovedTableNamePrefixes("t_um_");
		// 添加多个表名到黑名单
		generator.addBlacklist("login_log", "role_permission", "user_role");

		// 添加多个表名白名单
		generator.addWhitelist("wv_wa_lock");
		// 设置数据库方言
		if("sqlserver".equals(dbType)){
			generator.setDialect(new SqlServerDialect());
			generator.setMetaBuilder(new _SqlMetaBuilder(getDataSource()));
		}else if("mysql".equals(dbType)){
			generator.setDialect(new MysqlDialect());
		}
		// 生成
		generator.generate();
	}

}
